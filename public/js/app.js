var StreamCollection = Backbone.Collection.extend({
  stream: function(options) {        
    // Cancel any potential previous stream
    this.unstream();        
    var _update = _.bind(function() {
      this.fetch(options);
      this._intervalFetch = window.setTimeout(_update, options.interval || 1000);
    }, this);
    _update();
  },
  unstream: function() {
    window.clearTimeout(this._intervalFetch);
    delete this._intervalFetch;
  },    
  isStreaming : function() {
    return _.isUndefined(this._intervalFetch);   
  }
});

var PhotoModel = Backbone.Model.extend({});
var PhotoCollection = StreamCollection.extend({
  model: PhotoModel,
  initialize: function(options){
    this.options = options;
  },
  url: function(){
    return "/v1/tags/" + this.options.tag + "/media/recent?client_id=" + this.options.clientId;
  },
  parse: function(d) {
    this.options.pagination = d.pagination;
    return d.data;
  },
  comparator: function(d) {
    return - d.get('created_time');
  },
  recent: function(){
    return this.toJSON().splice(0,this.options.recent);
  },
  add : function(models, options) {
    var newModels = [];
    _.each(models, function(model) {
      if (_.isUndefined(this.get(model.id))) {
        newModels.push(model);    
      }
    }, this);
    return Backbone.Collection.prototype.add.call(this, newModels, options);
  }
});

var ThumbnailsView = Backbone.View.extend({
  el: $('#thumbs'),
  template: _.template($('#thumb-template').html()),
  initialize: function(){
    this.render();
    this.collection.stream({interval: 5000, add: true});
    this.collection.bind('add',function(model){
      this.$el.prepend(this.template(model.toJSON()));
      $('.thumb.hidden').removeClass('hidden');
      if ($('.thumb').length > 100) {
        $('.thumb').slice(100 - $('.thumb').length).each(function(i,e){
          $(e).remove();
        });
      }
    },this);
  },
  render: function(){
    this.collection.each(function(d){
      this.$el.append(this.template(d.toJSON()));
      $('.thumb.hidden').removeClass('hidden');
    },this);
  }
});

var CurrentPhotoView = Backbone.View.extend({
  el: $('#current-photo'),
  template: _.template($('#photo-template').html()),
  initialize: function(){
    this.queue = this.collection.recent();
    this.render();
    this.swapInterval = 7000;
    this.updateCurrentPhoto();
    this.reposition();
    var _self = this;
    $(window).on('resize',function(){
      _self.reposition();
    });
  },
  render: function(){
    this.$el.html(this.template(this.queue.pop()));
    $('.photo').removeClass('hidden');
  },
  reposition: function(){
    this.$el.css('top',($(window).height() - $('footer').height() - this.$el.height())/2);
  },
  updateCurrentPhoto: function(){
      var nextPhoto = this.queue.pop();
      if (nextPhoto) {
        this.$el.html(this.template(nextPhoto));      
        $('.photo').removeClass('hidden');
      } else {
        this.queue = this.collection.recent();
      }
      var _self = this;
      setTimeout(function(){ _self.updateCurrentPhoto() }, this.swapInterval);
  }
});

var TagView = Backbone.View.extend({
  el:$('#tag'),
  initialize:function(){
    this.$el.html(this.options.tag);
  }
});

function getQueryParam(param) {
  var query = window.location.search.substring(1);
  var params = query.split("&");
  for (var i = 0; i < params.length; i++) {
    var pair = params[i].split("=");
    if (pair[0] === param) {
      return unescape(pair[1]);
    }
  }
}

var options = {
  clientId: getQueryParam('clientId') || "9571ea7bf9d84fa98e4bcdb10719a73e",
  tag: getQueryParam('tag') || 'atlassian',
  recent: getQueryParam('recent') || 5
};

var tagView = new TagView({tag: options.tag});
var photoCollection = new PhotoCollection(options);
photoCollection.fetch().done(function(d){
  var currentPhotoView = new CurrentPhotoView({collection: photoCollection});
  function getMore(d,cnt) {
    photoCollection.fetch({add: true, data: {max_tag_id: d.pagination.next_max_tag_id}}).done(function(d){
      if(photoCollection.length < 100 && cnt < 5) {
        cnt += 1;
        getMore(d,cnt);
      } else {
        var thumbnailsView = new ThumbnailsView({collection: photoCollection});
      }
    });
  };
  getMore(d,0);
});
